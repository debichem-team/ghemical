// CUSTOM_APP.CPP

// Copyright (C) 2006 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "custom_app.h"

#include "local_i18n.h"

#include <stdlib.h>	// the definition for NULL...

/*################################################################################################*/

project * custom_app::prj = NULL;

custom_app::mtool custom_app::current_mouse_tool = custom_app::mtOrbitXY;
custom_app::select_mode custom_app::current_select_mode = custom_app::smAtom;

custom_app::custom_app(void) :
	base_app()
{
}

custom_app::~custom_app(void)
{
}

void custom_app::SetNewProject(void)
{
	// just reset the custom camera counter...
	// this is for graphical user interface only.
	
	custom_camera::ccam_counter = 0;
}

custom_app * custom_app::GetAppC(void)
{
	return dynamic_cast<custom_app *>(base_app::GetAppB());
}

project * custom_app::GetPrj(void)
{
	return prj;
}

custom_app::mtool custom_app::GetCurrentMouseTool(void)
{
	return current_mouse_tool;
}

custom_app::select_mode custom_app::GetCurrentSelectMode(void)
{
	return current_select_mode;
}

void custom_app::AddCamera(ogl_camera * cam)
{
	base_app::AddCamera(cam);
	
	custom_camera * ccam = dynamic_cast<custom_camera *>(cam);
	if (ccam != NULL) CameraAdded(ccam);
}

bool custom_app::RemoveCamera(ogl_camera * cam)
{
	bool success = base_app::RemoveCamera(cam);
	
	if (success)
	{
		custom_camera * ccam = dynamic_cast<custom_camera *>(cam);
		if (ccam != NULL) CameraRemoved(ccam);
	}
	
	return success;
}

bool custom_app::AddGlobalLight(ogl_light * light)
{
	bool success = base_app::AddGlobalLight(light);
	
	if (success)
	{
		LightAdded(light);
		project::selected_object = light;
		cout << _("Added global light.") << endl;
	}
	
	return success;
}

bool custom_app::AddLocalLight(ogl_light * light, ogl_camera * cam)
{
	bool success = base_app::AddLocalLight(light, cam);
	
	if (success)
	{
		LightAdded(light);
		project::selected_object = light;
		cout << _("Added local light.") << endl;
	}
	
	return success;
}

bool custom_app::RemoveLight(ogl_dummy_object * light)
{
	int n1 = IsLight(light);
	if (n1 < 0) return false;
	
	LightRemoved((ogl_light *) light);
	
	base_app::RemoveLight(light);
	
	return true;
}

bool custom_app::SelectLight(const ogl_dummy_object * obj)
{
	int n1 = IsLight(obj);
	if (n1 < 0) return false;
	
	prj->selected_object = light_vector[n1];
	
	return true;
}

/*################################################################################################*/

// eof
