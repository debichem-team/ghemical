// GTK_SETUP_DIALOG.CPP

// Copyright (C) 2002 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "gtk_setup_dialog.h"

#include "local_i18n.h"

#include <ghemical/engine.h>

#include <ghemical/eng1_qm.h>
#include <ghemical/eng1_qm.h>
#include <ghemical/eng1_sf.h>

#include <ghemical/eng2_qm_mm.h>

#include <gtk/gtk.h>

#include <sstream>
#include <iostream>
using namespace std;

/*################################################################################################*/

gtk_setup_dialog::gtk_setup_dialog(gtk_project * p1) :
	gtk_glade_dialog("glade/gtk_setup_dialog.glade")
{
	prj = p1;
	
	dialog = glade_xml_get_widget(xml, "setup_dialog");
	if (dialog == NULL)
	{
		cout << _("WARNING : setup_dialog : glade_xml_get_widget() failed!!!") << endl;
		return;
	}
	
	// initialize the widgets...
	// initialize the widgets...
	// initialize the widgets...
	
	GtkWidget * notebook_su = glade_xml_get_widget(xml, "notebook_su");
	GtkWidget * optmenu; GtkWidget * submenu; GtkWidget * item; i32u eng_index;
	
	GtkWidget * saved_optmenus[5];	// these are for setting the default engine...
	
	// setup1_qm
	// ^^^^^^^^^
	
	optmenu = glade_xml_get_widget(xml, "optionmenu_allQM_eng");
	submenu = gtk_menu_new(); saved_optmenus[0] = optmenu;
	
	for (eng_index = 0; eng_index < setup1_qm::static_GetEngineCount(); eng_index++)
	{
		item = gtk_menu_item_new_with_label(setup1_qm::static_GetEngineName(eng_index));
		gtk_widget_show(item); gtk_menu_append(GTK_MENU(submenu), item);
	}
	
	gtk_option_menu_set_menu(GTK_OPTION_MENU(optmenu), submenu);
	
	// setup1_mm
	// ^^^^^^^^^
	
	optmenu = glade_xml_get_widget(xml, "optionmenu_allMM_eng");
	submenu = gtk_menu_new(); saved_optmenus[1] = optmenu;
	
	for (eng_index = 0; eng_index < setup1_mm::static_GetEngineCount(); eng_index++)
	{
		item = gtk_menu_item_new_with_label(setup1_mm::static_GetEngineName(eng_index));
		gtk_widget_show(item); gtk_menu_append(GTK_MENU(submenu), item);
	}
	
	gtk_option_menu_set_menu(GTK_OPTION_MENU(optmenu), submenu);
	
	// setup1_sf
	// ^^^^^^^^^
	
	optmenu = glade_xml_get_widget(xml, "optionmenu_allSF_eng");
	submenu = gtk_menu_new(); saved_optmenus[2] = optmenu;
	
	for (eng_index = 0; eng_index < setup1_sf::static_GetEngineCount(); eng_index++)
	{
		item = gtk_menu_item_new_with_label(setup1_sf::static_GetEngineName(eng_index));
		gtk_widget_show(item); gtk_menu_append(GTK_MENU(submenu), item);
	}
	
	gtk_option_menu_set_menu(GTK_OPTION_MENU(optmenu), submenu);
	
	// setup2_qm_mm
	// ^^^^^^^^^^^^
	
	optmenu = glade_xml_get_widget(xml, "optionmenu_QMMM_eng");
	submenu = gtk_menu_new(); saved_optmenus[3] = optmenu;
	
	for (eng_index = 0; eng_index < setup2_qm_mm::static_GetEngineCount(); eng_index++)
	{
		item = gtk_menu_item_new_with_label(setup2_qm_mm::static_GetEngineName(eng_index));
		gtk_widget_show(item); gtk_menu_append(GTK_MENU(submenu), item);
	}
	
	gtk_option_menu_set_menu(GTK_OPTION_MENU(optmenu), submenu);
	
	// detect the type of model::current_setup object, and setup the default values...
	// detect the type of model::current_setup object, and setup the default values...
	// detect the type of model::current_setup object, and setup the default values...
	
	setup1_qm * su_allqm = dynamic_cast<setup1_qm *>(prj->GetCurrentSetup());
	setup1_mm * su_allmm = dynamic_cast<setup1_mm *>(prj->GetCurrentSetup());
	setup1_sf * su_allsf = dynamic_cast<setup1_sf *>(prj->GetCurrentSetup());
	
	setup2_qm_mm * su_qmmm = dynamic_cast<setup2_qm_mm *>(prj->GetCurrentSetup());
	
	// the defaults for the subpages...
	// the defaults for the subpages...
	// the defaults for the subpages...
	
	if (su_allqm == NULL)
	{
		GtkWidget * entry_totchrg = glade_xml_get_widget(xml, "entry_allQM_totchrg");		// total charge
		gtk_entry_set_text(GTK_ENTRY(entry_totchrg), "+0");
		
		GtkWidget * entry_spinmult = glade_xml_get_widget(xml, "entry_allQM_spinmult");		// spin multiplicity
		// todo!!! todo!!! todo!!! todo!!! todo!!! todo!!! todo!!!
		gtk_entry_set_text(GTK_ENTRY(entry_spinmult), "1");	// not yet implemented!
		gtk_widget_set_sensitive (entry_spinmult, false);	// not yet implemented!
		// todo!!! todo!!! todo!!! todo!!! todo!!! todo!!! todo!!!
	}
	
	if (su_allmm == NULL)
	{
		//GtkWidget * entry_dimx = glade_xml_get_widget(xml, "entry_allmm_dimx");	// pbc dim x
		//gtk_entry_set_text(GTK_ENTRY(entry_dimx), "1.500");
		//GtkWidget * entry_dimy = glade_xml_get_widget(xml, "entry_allmm_dimy");	// pbc dim y
		//gtk_entry_set_text(GTK_ENTRY(entry_dimy), "1.500");
		//GtkWidget * entry_dimz = glade_xml_get_widget(xml, "entry_allmm_dimz");	// pbc dim z
		//gtk_entry_set_text(GTK_ENTRY(entry_dimz), "1.500");
	}
	
	if (su_allsf == NULL)
	{
	}
	
	if (su_qmmm == NULL)
	{
	}
	
	// the settings specific to the model::current_setup object...
	// the settings specific to the model::current_setup object...
	// the settings specific to the model::current_setup object...

	if (su_allqm != NULL)
	{
		gtk_option_menu_set_history(GTK_OPTION_MENU(saved_optmenus[0]), su_allqm->GetCurrEngIndex());
		gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook_su), 0);
		
		GtkWidget * entry_totchrg = glade_xml_get_widget(xml, "entry_allQM_totchrg");		// total charge
		GtkWidget * entry_spinmult = glade_xml_get_widget(xml, "entry_allQM_spinmult");		// spin multiplicity
		
		ostringstream str;
		
		str.setf(ios::showpos); str << prj->GetQMTotalCharge() << ends;
		gtk_entry_set_text(GTK_ENTRY(entry_totchrg), str.str().c_str());
		
		// todo!!! todo!!! todo!!! todo!!! todo!!! todo!!! todo!!!
		gtk_entry_set_text(GTK_ENTRY(entry_spinmult), "1");	// not yet implemented!
		gtk_widget_set_sensitive (entry_spinmult, false);	// not yet implemented!
		// todo!!! todo!!! todo!!! todo!!! todo!!! todo!!! todo!!!
	}
	else if (su_allmm != NULL)
	{
		gtk_option_menu_set_history(GTK_OPTION_MENU(saved_optmenus[1]), su_allmm->GetCurrEngIndex());
		gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook_su), 1);
		
		GtkWidget * checkbutton_amber = glade_xml_get_widget(xml, "checkbutton_allMM_amber");		// exceptions flag
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbutton_amber), (su_allmm->GetExceptions() ? TRUE : FALSE));
		
		//GtkWidget * entry_dimx = glade_xml_get_widget(xml, "entry_allmm_dimx");	// pbc dim x
		//gtk_entry_set_text(GTK_ENTRY(entry_dimx), "???");
		//GtkWidget * entry_dimy = glade_xml_get_widget(xml, "entry_allmm_dimy");	// pbc dim y
		//gtk_entry_set_text(GTK_ENTRY(entry_dimy), "???");
		//GtkWidget * entry_dimz = glade_xml_get_widget(xml, "entry_allmm_dimz");	// pbc dim z
		//gtk_entry_set_text(GTK_ENTRY(entry_dimz), "???");
	}
	else if (su_allsf != NULL)
	{
		gtk_option_menu_set_history(GTK_OPTION_MENU(saved_optmenus[2]), su_allsf->GetCurrEngIndex());
		gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook_su), 2);
	}
	else if (su_qmmm != NULL)
	{
	//	gtk_option_menu_set_history(GTK_OPTION_MENU(saved_optmenus[3]), su_qmmm->current_eng_index);
		gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook_su), 3);
	}
	else
	{
		assertion_failed(__FILE__, __LINE__, "setup class unknown.");
	}
	
	// connect the handlers...
	
	glade_xml_signal_connect_data(xml, "on_dialog_destroy", (GtkSignalFunc) handler_Destroy, (gpointer) this);
	
	glade_xml_signal_connect_data(xml, "on_button_ok_clicked", (GtkSignalFunc) handler_ButtonOK, (gpointer) this);
	glade_xml_signal_connect_data(xml, "on_button_cancel_clicked", (GtkSignalFunc) handler_ButtonCancel, (gpointer) this);
	
	gtk_dialog_run(GTK_DIALOG(dialog));	// MODAL
	gtk_widget_destroy(dialog);		// MODAL
}

gtk_setup_dialog::~gtk_setup_dialog(void)
{
}

void gtk_setup_dialog::handler_Destroy(GtkWidget *, gpointer data)			// not really needed...
{
	gtk_setup_dialog * ref = (gtk_setup_dialog *) data;
	//cout << "handler_Destroy() : ref = " << ref << endl;
}

void gtk_setup_dialog::handler_ButtonOK(GtkWidget *, gpointer data)
{
	gtk_setup_dialog * ref = (gtk_setup_dialog *) data;
	//cout << "handler_ButtonOK() : ref = " << ref << endl;
	
	// read in and process the user's settings...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	GtkWidget * notebook_su = glade_xml_get_widget(ref->xml, "notebook_su");
	GtkWidget * optmenu; GtkWidget * submenu; GtkWidget * item; int index = NOT_DEFINED;
	
	const gint current_page = gtk_notebook_get_current_page(GTK_NOTEBOOK(notebook_su));
	
	setup1_sf::SFmode SFmd;
	
	if (current_page == 0)
	{
		optmenu = glade_xml_get_widget(ref->xml, "optionmenu_allQM_eng");
		submenu = gtk_option_menu_get_menu(GTK_OPTION_MENU(optmenu));
		item = gtk_menu_get_active(GTK_MENU(submenu));
		
		index = g_list_index(GTK_MENU_SHELL(submenu)->children, item);
		
	//delete ref->prj->current_setup;					// get rid of the old setup...
	//ref->prj->current_setup = new setup1_qm(ref->prj);			// ...and make a new one.
		ref->prj->ReplaceCurrentSetup(new setup1_qm(ref->prj));
		ref->prj->GetCurrentSetup()->SetCurrEngIndex(index);		// set the requested engine.
		
		// handle the total charge
		
		GtkWidget * entry_totchrg = glade_xml_get_widget(ref->xml, "entry_allQM_totchrg");
		const gchar * buffer = gtk_entry_get_text(GTK_ENTRY(entry_totchrg));
		istringstream istr(buffer); i32s value; istr >> value;
		ref->prj->SetQMTotalCharge(value);
		
		// TODO : handle the multiplicity...
	}
	else if (current_page == 1)
	{
		optmenu = glade_xml_get_widget(ref->xml, "optionmenu_allMM_eng");
		submenu = gtk_option_menu_get_menu(GTK_OPTION_MENU(optmenu));
		item = gtk_menu_get_active(GTK_MENU(submenu));
		
		index = g_list_index(GTK_MENU_SHELL(submenu)->children, item);
		
	//delete ref->prj->current_setup;					// get rid of the old setup...
	//ref->prj->current_setup = new setup1_mm(ref->prj);			// ...and make a new one.
		ref->prj->ReplaceCurrentSetup(new setup1_mm(ref->prj));
		ref->prj->GetCurrentSetup()->SetCurrEngIndex(index);		// set the requested engine.
		
		// handle the exceptions flag
		
		GtkWidget * checkbutton_amber = glade_xml_get_widget(ref->xml, "checkbutton_allMM_amber");
		bool exceptions = (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbutton_amber)) == TRUE ? true : false);
		dynamic_cast<setup1_mm *>(ref->prj->GetCurrentSetup())->SetExceptions(exceptions);
	}
	else if (current_page == 2)
	{
		optmenu = glade_xml_get_widget(ref->xml, "optionmenu_allSF_eng");
		submenu = gtk_option_menu_get_menu(GTK_OPTION_MENU(optmenu));
		item = gtk_menu_get_active(GTK_MENU(submenu));
		
		index = g_list_index(GTK_MENU_SHELL(submenu)->children, item);
		
		switch (index)
		{
			case 0:
			case 1:
			SFmd = setup1_sf::modeUA;
			break;
			
			case 2:
			case 3:
			SFmd = setup1_sf::modeP5;
			break;
			
			case 4:
			case 5:
			SFmd = setup1_sf::modeP3;
			break;
			
			default:
			assertion_failed(__FILE__, __LINE__, "cannot set SFmode!");
		}
		
	//delete ref->prj->current_setup;					// get rid of the old setup...
	//ref->prj->current_setup = new setup1_sf(ref->prj, !index);		// ...and make a new one.
		ref->prj->ReplaceCurrentSetup(new setup1_sf(ref->prj, SFmd));
		ref->prj->GetCurrentSetup()->SetCurrEngIndex(index);		// set the requested engine.
	}
	else if (current_page == 3)
	{
	/*	optmenu = glade_xml_get_widget(ref->xml, "optionmenu_QMMM_eng");
		submenu = gtk_option_menu_get_menu(GTK_OPTION_MENU(optmenu));
		item = gtk_menu_get_active(GTK_MENU(submenu));
		
		index = g_list_index(GTK_MENU_SHELL(submenu)->children, item);
		
		delete ref->prj->current_setup;						// get rid of the old setup...
		ref->prj->current_setup = new setup2_qm_mm(ref->prj);			// ...and make a new one.
		ref->prj->GetCurrentSetup()->current_eng_index = index;			// set the requested engine.	*/
	}
	else if (current_page == 4)
	{
	/*	optmenu = glade_xml_get_widget(ref->xml, "optionmenu_MMSF_eng");
		submenu = gtk_option_menu_get_menu(GTK_OPTION_MENU(optmenu));
		item = gtk_menu_get_active(GTK_MENU(submenu));
		
		index = g_list_index(GTK_MENU_SHELL(submenu)->children, item);
		
		delete ref->prj->current_setup;						// get rid of the old setup...
		ref->prj->current_setup = new setup2_mm_sf(ref->prj);			// ...and make a new one.
		ref->prj->GetCurrentSetup()->current_eng_index = index;			// set the requested engine.	*/
	}
	else
	{
		assertion_failed(__FILE__, __LINE__, "invalid current page.");
	}
	
	// report the new settings to log...
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	ostringstream str1;
	str1 << _("Changed the Setup for calculations ");
	str1 << _("(setup = ") << ref->prj->GetCurrentSetup()->GetClassName_lg();
	
	if (current_page == 2)
	{
		str1 << "mode = ";
		switch (SFmd)
		{
			case setup1_sf::modeUA:		str1 << "UA"; break;
			case setup1_sf::modeP5:		str1 << "P5"; break;
			case setup1_sf::modeP3:		str1 << "P3"; break;
			
			default:
			assertion_failed(__FILE__, __LINE__, "cannot report SFmode!");
		}
	}
	
	str1 << _(", engine = ") << ref->prj->GetCurrentSetup()->GetEngineName(ref->prj->GetCurrentSetup()->GetCurrEngIndex());
	str1 << ")." << endl << ends;
	
	ref->prj->PrintToLog(str1.str().c_str());
}

void gtk_setup_dialog::handler_ButtonCancel(GtkWidget *, gpointer data)		// not really needed...
{
	gtk_setup_dialog * ref = (gtk_setup_dialog *) data;
	//cout << "handler_ButtonCancel() : ref = " << ref << endl;
}

/*################################################################################################*/

// eof
