// ELDVIEW_WCL.H : write a short description here...

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "ghemicalconfig2.h"

#ifndef ELDVIEW_WCL_H
#define ELDVIEW_WCL_H

#include "pangofont_wcl.h"

/*################################################################################################*/

class eldview_wcl :
	public pangofont_wcl
{
	protected:
	
	bool reset_needed;
	
	public:
	
	eldview_wcl(void);
	virtual ~eldview_wcl(void);
	
	void SetCenterAndScale(void);
	
	void ButtonEvent(int, int);		// virtual
	void MotionEvent(int, int);		// virtual
	
	void UpdateWnd(void);			// virtual
	
	void InitGL(void);			// virtual
	void RenderGL(rmode);			// virtual
};

/*################################################################################################*/

#endif	// ELDVIEW_WCL_H

// eof
