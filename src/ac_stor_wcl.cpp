// AC_STOR_WCL.CPP

// Copyright (C) 2006 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "ac_stor_wcl.h"

#include "project.h"
#include "custom_app.h"

/*################################################################################################*/

ac_stor_wcl::ac_stor_wcl(ogl_camera * cam) :
	pangofont_wcl(cam)
{
}

ac_stor_wcl::~ac_stor_wcl(void)
{
	for (int i = 0;i < (int) acv.size();i++)
	{
		if (acv[i] != NULL)
		{
			delete[] acv[i];
			acv[i] = NULL;
		}
	}
}

int ac_stor_wcl::StoreAC(engine * eng)
{
	if (eng == NULL) return NOT_DEFINED;
	
	if (!acv.size())	// is this the 1st set???
	{
		if (av.size() != 0)
		{
			assertion_failed(__FILE__, __LINE__, "av is not empty.");
		}
		
		atom ** atmtab = eng->GetSetup()->GetAtoms();
		for (int i = 0;i < eng->GetSetup()->GetAtomCount();i++)
		{
			av.push_back(atmtab[i]);
		}
	}
	else
	{
		if (av.size() != eng->GetSetup()->GetAtomCount())
		{
			assertion_failed(__FILE__, __LINE__, "av.size() is bad.");
		}
	}
	
	float * newXYZ = new float[av.size() * 3];
	
	// store the atom coordinates for later viewing...
	// just assume that the atoms are not re-ordered.
	// the number of atoms is stored and checked.
	
	for (int i = 0;i < (int) av.size() * 3;i++)
	{
		newXYZ[i] = eng->ReadCRD(i);
	}
	
	int retval = (int) acv.size();
	acv.push_back(newXYZ);
	
	return retval;
}

int ac_stor_wcl::StoreAC(model * mdl, int cset)
{
	if (mdl == NULL) return NOT_DEFINED;
	
	if (!acv.size())	// is this the 1st set???
	{
		if (av.size() != 0)
		{
			assertion_failed(__FILE__, __LINE__, "av is not empty.");
		}
		
		atom ** atmtab = mdl->GetCurrentSetup()->GetAtoms();
		for (int i = 0;i < mdl->GetCurrentSetup()->GetAtomCount();i++)
		{
			av.push_back(atmtab[i]);
		}
	}
	else
	{
		if (av.size() != mdl->GetCurrentSetup()->GetAtomCount())
		{
			assertion_failed(__FILE__, __LINE__, "av.size() is bad.");
		}
	}
	
	float * newXYZ = new float[av.size() * 3];
	
	// store the atom coordinates for later viewing...
	// just assume that the atoms are not re-ordered.
	// the number of atoms is stored and checked.
	
	for (int i = 0;i < (int) av.size();i++)
	{
		const fGL * xyz = av[i]->GetCRD(cset);
		
		newXYZ[i * 3 + 0] = xyz[0];
		newXYZ[i * 3 + 1] = xyz[1];
		newXYZ[i * 3 + 2] = xyz[2];
	}
	
	int retval = (int) acv.size();
	acv.push_back(newXYZ);
	
	return retval;
}

void ac_stor_wcl::ShowAC(int index)
{
	if (index < 0 || index >= (int) acv.size()) return;
	
	project * prj = custom_app::GetAppC()->GetPrj();
	
	if (acv[index] != NULL && av.size() == prj->GetCurrentSetup()->GetAtomCount())
	{
		for (int i = 0;i < (int) av.size();i++)
		{
			const float x = acv[index][i * 3 + 0];
			const float y = acv[index][i * 3 + 1];
			const float z = acv[index][i * 3 + 2];
			
			const int cset = 0;	// ???
			
			av[i]->SetCRD(cset, x, y, z);
		}
		
		prj->UpdateAllGraphicsViews();
		
		// also make sure that if user calculates any results, the new structure will be used!
		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		
		prj->GetCurrentSetup()->DiscardCurrentEngine();
	}
}

/*################################################################################################*/

// eof
