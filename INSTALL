	REQUIREMENTS:
	-------------

The ghemical package is dependent on "libghemical" package, in addition
to the packages/files that are needed to compile and run programs at
the GTK2 platform. Also ghemical is optionally dependent on "OpenBabel"
packages (for file import/export features).

You can get "libghemical-2.95.tgz" or newer from

	http://bioinformatics.org/ghemical/download.html

You can get "openbabel-2.0.0.tar.gz" or newer from

	http://openbabel.sourceforge.net/

Other software/packages that are needed to compile and run this program
are:

	g++			gcc version 4.1.2 or later
	make			GNU Make version 3.76.1
	pkg-config		pkg-config-0.15
	
	glib			glib-2.0 version 2.6.0 or newer
	gtk+			gtk+-2.0 version 2.6.0 or newer
	gtkglext		gtkglext-1.0 version 1.0.5 or newer
	libglade		libglade-2.0 version 2.4.0 or newer
	gthread			gthread-2.0 version 2.6.0 or newer (optional)

In short, any up-to-date Linux installation like Redhat, Mandrake
or Debian should work, if the necessary development packages (that
contain the header files) are also present, in addition to the standard
packages (that contain the libraries).


	INSTALLATION:
	-------------

Simple set of commands

	./autogen.sh
	./configure
	make
	make install		[run as root user]

will produce an executable program in which all the optional dependencies
mentioned above are disabled. You can enable the options by adding the
following statements to the configuration script command line:

	./configure --enable-openbabel

If you wish to disable multithreading you may use:

	./configure --enable-threads=no

For more options and information you can try

	./configure --help

If at configuration step you get stuck and see some error messages about
PKG_CONFIG, please try the following tricks (and re-try ./configure):

	export PKG_CONFIG=pkg-config
	export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig

Also if problems appear you might want to try updating the configuration
files using commands at the top of the source tree (and re-try ./configure):

	aclocal
	autoconf
	autoheader

This is an example how to set different compiler versions in Makefiles:

	CC=gcc-3.4 CXX=g++-3.4 ./configure

The "make install"-command will copy the executable and some extra files to
their proper places so that the program can be run anywhere. After you have
installed the program you can click the item "Help/Help" from the main menu
to read the User's Manual (highly recommended).

In the "examples"-directory there are some useful example molecules, and
in the "small-utilities"-directory there are... well, some small utilities.

Enjoy!!!

