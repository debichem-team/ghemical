#!/bin/sh

## this can be skipped if you don't want i18n.
##^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
echo "Running intltoolize"
intltoolize --force --copy

echo "Running libtoolize"
libtoolize --force --copy

echo "Removing autom4te.cache"
rm -fr autom4te.cache
rm -f aclocal.m4

##OSTYPE=`uname -s`
##AMVER="-1.7"
##AMFLAGS="--add-missing"
##if test "$OSTYPE" = "IRIX" -o "$OSTYPE" = "IRIX64"; then
##   AMFLAGS=$AMFLAGS" --include-deps";
##fi
##echo "Running aclocal$AMVER"
##rm -fr autom4te.cache ; aclocal$AMVER
##echo "Running autoheader"
##autoheader
##echo "Running automake$AMVER"
##automake$AMVER $AMFLAGS
##echo "Running autoconf"
##autoconf

echo "Running autoreconf"

##export AUTOCONF=autoconf-2.13
##export AUTOHEADER=autoheader-2.13

##export ACLOCAL=aclocal-1.4
##export AUTOMAKE=automake-1.4

autoreconf -v -f

echo "======================================"
echo "Now you are ready to run './configure'"
echo "======================================"

