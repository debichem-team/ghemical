Summary: The MM and QM calculations frontend.
Name: ghemical
Version: 2.60
Release: 1
License: GPL
Group: Science/Chemistry
Source: http://www.uku.fi/~thassine/projects/download/%{name}-%{version}.tar.gz
URL: http://www.uku.fi/~thassine/ghemical/
Icon: ghemical.xpm
Vendor: Tommi Hassinen et al.
Packager: Radek Liboska <liboska@uochb.cas.cz>

%description
Ghemical is a computational chemistry software package released under the 
GNU GPL.
Ghemical is written in C++. It has a graphical user interface (in fact, 
a couple of them), and it supports both quantum-mechanics (semi-empirical 
and ab initio) models and molecular mechanics models (there is an experimental
Tripos 5.2-like force field for organic molecules). Also a tool for reduced 
protein models is included. Geometry optimization, molecular dynamics
and a large set of visualization tools are currently available.

%prep
rm -rf $RPM_BUILD_ROOT

%setup
%build
%configure --enable-openbabel
make

%install
make install-strip prefix=$RPM_BUILD_ROOT/%{_prefix}

%files
%defattr(-,root,root)
%doc docs/user-docs
%{_bindir}/ghemical
%{_datadir}/%{name}/%{version}/examples

%clean
rm -rf $RPM_BUILD_ROOT

%changelog
* Thu Oct 12 2006 Radek Liboska <liboska@uochb.cas.cz> fc5
- ver 2.10, fedora core 5
